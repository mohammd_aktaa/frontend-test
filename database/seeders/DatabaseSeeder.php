<?php

namespace Database\Seeders;

use App\Models\Post;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        \App\Models\Post::factory(50)->create()->each(function (Post $post) {
            $faker = Factory::create();
            $fakerAr = Factory::create('ar_SA');
            $post->translations()->insert([
                [
                    'lang' => 'en',
                    'title' => $faker->text,
                    'description' => $faker->sentence(50),
                    'parent_id' => $post->id
                ],
                [
                    'lang' => 'ar',
                    'title' => $fakerAr->title . ' ' . $fakerAr->name,
                    'description' => $fakerAr->realText,
                    'parent_id' => $post->id
                ],
            ]);
        });
    }
}
