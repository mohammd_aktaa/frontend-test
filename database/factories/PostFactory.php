<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $sizes = [
            '240', '320', '400',
            '480', '560', '640',
            '720', '800', '880',
            '960',
        ];
        return [
            'image' => $this->faker->imageUrl(width: $sizes[rand(0, 9)], height: $sizes[rand(0, 9)], category: 'fruits'),
            'user_id' => rand(1, 20),
            'slug' => $this->faker->slug
        ];
    }
}
