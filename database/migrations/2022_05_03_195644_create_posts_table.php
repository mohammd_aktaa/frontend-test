<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->string('slug');
            $table->timestamps();

            $table->foreignIdFor(\App\Models\User::class)->references('id')->on('users')->cascadeOnDelete();
        });
        Schema::create('posts_i18n', function (Blueprint $table) {
            $table->id();
            $table->string('lang', 4);
            $table->string('title');
            $table->text('description');

            $table->foreignIdFor(\App\Models\Post::class, 'parent_id')->references('id')->on('posts')->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
