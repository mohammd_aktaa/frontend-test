## How to run project

### Laravel Project

#### - Installation

First you need to install laravel from official docs [here](https://laravel.com/docs/9.x/installation), or you already have on you just need to install php packages by running `composer install`  You can install it from
here [composer.exe](https://getcomposer.org/download) ,then run migrations with some seeds `php artisan migrate --seed`
, I have created some factories to insert fake data you can check them in [factories](./database/factories) folder.

Second to run project you can use built in server by running this command `php artisan serve` or use one of these
applications [wamp](https://www.wampserver.com/en/) ,[Xampp](https://www.apachefriends.org/download.html)
,[Laragon](https://laragon.org/download/index.html) , I prefer Laragon because it's doing everything from updating hosts
file and making pretty url for projects.

#### - Configuration

To localize project i created middleware to change app locale based on cookie `app_lang` check it
here [LocalizeApp](./app/Http/Middleware/LocalizeApp.php), and add it to `$middlewareGroups` array to `'api'` index ,so
it will be applied to all api [routes](./routes/api.php).

The main view i used [app.blade.php](./resources/views/app.blade.php), which will have the assets from our angular
project which shared them in [AngularBuildServiceProvider](./app/Providers/AngularBuildServiceProvider.php) in this file
i get the assets from `stats.json` which generated from angular project.

in `<head>` tag

```php
 @env('production')
        @if (isset($assets) && count($assets))
            <link rel="stylesheet" href="/front/{{ $assets['styles'] }}">
        @endif
 @endenv
```

in `<body>` tag

```php

@env('production')
    @if (isset($assets) && count($assets))
        <script src="/front/{{ $assets['runtime'] }}" defer></script>
        <script src="/front/{{ $assets['polyfills'] }}" defer></script>
        <script src="/front/{{ $assets['main'] }}" defer></script>
    @endif
    {{-- This'll load the development assets when in dev mode --}}
@else
    <script src="/front/runtime.js" defer></script>
    <script src="/front/polyfills.js" defer></script>
    <script src="/front/styles.js" defer></script>
    <script src="/front/vendor.js" defer></script>
    <script src="/front/main.js" defer></script>
@endenv
```

<small>note: Will talk about this one in angular project installation.</small>


#### - Translation

You can check translations files in [lang](./lang) folder, nothing new used just the built-in functionality in laravel.


### Angular project

#### - Installation

If you are using node js for the first time you need to install  _node.js_ from [here](https://nodejs.org/en/download/).

and now we need to install angular cli so we can initialize new projects and creating other stuff related to angular
project like (Components, Pipes, Services, etc....), you can find it [here](https://angular.io/guide/setup-local).

If you are using a fresh project you need to run this command `ng new APP_NAME` it will create a fresh angular project,
or if you already have angular project without its dependencies you need to run `npm i --save` in your project folder.
In our case we need to go to our angular project folder which is `demo-frontend` so you can simply run the
following `cd demo-frontend && npm i --save`.

`--save` flag to save your packages' versions in cache.

#### - Configuration

We need configure our application, so laravel can easily use the assets files of our front end project, here how we made
it in [angular.json](./demo-frontend/angular.json) we just changed the configuration for `development` and `production`:

```json
  "development": {
     "statsJson": true,
     "deployUrl": "front/"
   }
```

```json
   "production": {
      "statsJson": true,
      "deployUrl": "front/"
    },
```

and we changed the build options to be like this:

```json
  "outputPath": "../public/front",
  "index": "",
```

So it will know the output path will be in th parent project in `public` folder, and we emptied the index to not making
anything on it since we will not use it.

what we need from this two options:

- `deployUrl` is to deploy our assets in this url prefix `front/` so when we lazyloading other modules and styles
  nothing went wrong.
- `statsJson` to generate [stats.json](./public/front/stats.json) which allow us to get generated files from it.

#### - Global Classes, Interfaces and services

In [core](./demo-frontend/src/app/core) folder you can see:
1. `interceptors` which includes _[`api.interceptor`](demo-frontend/src/app/core/interceptors/api.interceptor.ts)_ to override the request before call it, we used it to add the access token for authenticated users and to add prefix to our apis urls we stored it in [environment](./demo-frontend/src/environments/environment.ts) file.
    We added it to app.modules.ts
```typescript
  providers: [
    {
      multi: true,
      useClass: ApiInterceptor,
      provide: HTTP_INTERCEPTORS
    }
  ]
```
2. `interfaces` which includes most common used interfaces
3. `resolvers` which includes `authenticated-user` which is responsible to get the auth user if it was authenticated.
4. `services` which includes :
   1. `alert` to show and hide alerts in success and error responses.
   2. `base` it contains the main methods of ajax like [`post`, `get`,...].
   3. `language` the logic where we do when we change the language you can check `changeLang` method.


#### - Styling

I used [Tailwindcss](https://tailwindcss.com/) to style the demo, it's a utility-first CSS framework packed with classes
that can be composed to build any design, directly in your markup.

You can check the docs on how to install it on angular
project [installation](https://tailwindcss.com/docs/guides/angular).

#### - Translation

Also i made this demo multi lang i used [ngx-translate](https://github.com/ngx-translate/core) you can figure it out
with provided link.

1. install package `npm install @ngx-translate/core --save`
2. add it to your module import array we also passed some config to TranslateModule like defaultLang and loader which we
   override it to specify the folder from where to get translations or if it will be online in another website, in our
   demo you can look at [app.module.ts](./demo-frontend/src/app/app.module.ts):

```ts
    imports:[
        ...,
        TranslateModule.forRoot({
            useDefaultLang: true,
            defaultLanguage: 'en',
            loader: {
                provide: TranslateLoader,
                useClass: AppTranslateLoader,
            }
        })
    ]
```

```ts
AppTranslateLoader

export class AppTranslateLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
        return from(import(`../assets/i18n/${lang}.json`));
    }
}
```

#### - Routing

The main routes are set in [app-routing.module.ts](./demo-frontend/src/app/app-routing.module.ts) which are lazyloading our modules to keep our bundles size as small as we can
```typescript
const routes: Routes = [
  {
    path: '',
    redirectTo: '/posts',
    pathMatch: 'full'
  },
  {
    path: 'posts',
    loadChildren: () => import('./modules/posts/posts.module').then(m => m.PostsModule),
    resolve: {user: AuthenticatedUserResolver}
  },
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
  }
];
```
you can check we used `resolve` here to get the auth user in this route.

We also have guards in [auth](./demo-frontend/src/app/modules/auth/guards) module to make our routes protected against non-logged-in users or to prevent logged-in users from entering `login` and `register` pages. You can check out this ex from auth module.


```typescript
const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [GuestGuard]
  }
]
```

#### - Demo Layout

This is the structure of our enter page [app.component.html](./demo-frontend/src/app/app.component.html)

```html
<app-header></app-header>
<router-outlet></router-outlet>
<app-footer></app-footer>
<app-notification></app-notification>
```
`header` and `footer` components are under [`layout`](./demo-frontend/src/app/layout) folder.

```html
<router-outlet></router-outlet>
```
Used to show our pages which are added in routes array here.

```html
<app-notification></app-notification>
```
[Notification](./demo-frontend/src/app/modules/shared/components/notification) Used to show our alerts which is triggered to be shown from `alert` service.

##### Posts page

we used masonry grid library to create masonry gallery for our posts, to install it :
1. `npm i masonry-layout ngx-masonry --save`
2. add `BrowserAnimationsModule` to imports in `app.module.ts`
3. add `NgxMasonryModule` to imports in `posts.module.ts`

now you are able to use it in your pages, here we used it in [masonry-grid](./demo-frontend/src/app/modules/posts/components/masonry-grid/masonry-grid.component.html) component in our [index](demo-frontend/src/app/modules/posts/containers/index/index.component.html) page

```html
<ngx-masonry #masonry [options]="masonryOptions" [updateLayout]="true">
  <div ngxMasonryItem [prepend]="false" class="masonry-item rounded-lg shadow-lg overflow-hidden group"
       *ngFor="let item of posts">
    <img [src]="item.image_url" masonrylazy loading="lazy" class="object-cover max-w-full" [alt]="item | translateAttribute:'title'">
    <div class="masonry-item-details absolute inset-0 transition-opacity bg-gradient-to-b from-transparent to-gray-800 opacity-0 group-hover:opacity-100">
      <span class="edit absolute text-white top-2 left-2 rtl:right-2 rtl:left-auto cursor-pointer border-b border-b-0 hover:border-b-2 transition-all" (click)="openFormDialog(item)" canMakeAction [post]="item">{{'BUTTONS.EDIT' | translate}}</span>
      <a routerLink="/posts/{{item.slug}}" class="title absolute text-white text-lg font-bold bottom-2 left-2 rtl:right-2 rtl:left-auto">{{item | translateAttribute:'title'}}</a>
      <span class="edit absolute text-white bottom-2 right-2 rtl:left-2 rtl:right-auto cursor-pointer border-b border-b-0 hover:border-b-2 transition-all" (click)="openDeleteDialog(item)" canMakeAction [post]="item">{{'BUTTONS.DELETE' | translate}}</span>
    </div>
  </div>
</ngx-masonry>
```

#### #[Posts Service](demo-frontend/src/app/modules/posts/services/posts.service.ts)

as you can see we extend from `BaseService` and used them in our specified functions like `listPosts` , `deletePost` , ...

#####  Directives

[`canMakeAction`](./demo-frontend/src/app/modules/shared/directives/can-make-action.directive.ts) this directive allow us to hide and show elements based on auth user and if he is the owner of this post
```html
<span class="edit absolute text-white bottom-2 right-2 rtl:left-2 rtl:right-auto cursor-pointer border-b border-b-0 hover:border-b-2 transition-all" (click)="openDeleteDialog(item)" canMakeAction [post]="item">
```

#####  Pipes

[`translate-attribute`](./demo-frontend/src/app/modules/shared/pipes/translate-attribute.pipe.ts) this pipe allow us to get translated data based on provided attribute
```html
 <h3 class="text-gray-800 font-bold text-xl">{{post | translateAttribute:'title'}}</h3>
```


### - Shared Module

We use to put all common components, pipes, directives and etc....

That's it.
