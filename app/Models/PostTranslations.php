<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $title
 * @property string $lang
 * @property string $description
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Query\Builder
 */
class PostTranslations extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'posts_i18n';

    protected $fillable = [
        'title',
        'description',
        'parent_id',
        'lang',
    ];

}
