<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AngularBuildServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if (config('app.env') === 'production') {
            $this->getAngularAssets();
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    private function getAngularAssets()
    {
        $path = public_path('front') . '/stats.json';
        $assets = [];

        try {
            $json = json_decode(file_get_contents($path), true);

            if (isset($json['assets']) && count($json['assets'])) {
                foreach ($json['assets'] as $asset) {
                    $name = $asset['name'];

//                    dump($name);
                    if ($asset['chunkNames'] && count($asset['chunkNames'])) {
                        $assets[$asset['chunkNames'][0]] = $name;
                    } else {
                        $assets[$name] = $name;
                    }
                }
            }
        } catch (\Exception $e) {
        }

        view()->share('assets', $assets);
    }
}
