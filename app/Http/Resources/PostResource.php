<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Post
 */
class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image_url' => $this->image_url,
            'slug' => $this->slug,
            'user' => UserResource::make($this->whenLoaded('user')),
            'translations' => PostTranslationsResource::collection($this->whenLoaded('translations')),
        ];
    }
}
