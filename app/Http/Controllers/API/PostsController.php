<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdatePostRequest;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\BaseCollection
     */
    public function index(Request $request)
    {
        $posts = Post::query()->with(['user', 'translations'])->paginate($request->get('perPage', 20), '*', 'page', $request->get('page', 1));
        return BaseCollection::make($posts, PostResource::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreUpdatePostRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreUpdatePostRequest $request)
    {
        $data = $request->validated();
        try {
            DB::beginTransaction();
            $data['image'] = (new Post)->saveImage($request->file('image'));
            $data['user_id'] = auth()->id();
            $title = $request->collect('translations')->where('lang', 'en')->first()['title'];
            $titleAr = $request->collect('translations')->where('lang', 'ar')->first()['title'];
            $data['slug'] = Str::slug($title ?? $titleAr);
            $post = Post::create($data);
            foreach ($data['translations'] as $translation) {
                $translation['parent_id'] = $post->id;
            }
            $data['translations'] = collect($data['translations'])->map(function ($item) use ($post) {
                $item['parent_id'] = $post->id;
                return $item;
            });
            $post->translations()->insert($data['translations']->toArray());
            DB::commit();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return response()->json(['message' => __('app.saved_successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Post $post
     *
     * @return \App\Http\Resources\PostResource
     */
    public function show(Post $post)
    {
        return PostResource::make($post->load(['user', 'translations']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\StoreUpdatePostRequest $request
     * @param \App\Models\Post $post
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function update(StoreUpdatePostRequest $request, Post $post)
    {
        $this->authorize('update', $post);
        try {
            DB::beginTransaction();
            $data = $request->validated();
            if ($request->hasFile('image'))
                $data['image'] = (new Post)->saveImage($request->file('image'));
            $title = $request->collect('translations')->where('lang', 'en')->first()['title'];
            $titleAr = $request->collect('translations')->where('lang', 'ar')->first()['title'];
            $data['slug'] = Str::slug($title ?? $titleAr);
            $post->update($data);
            foreach ($data['translations'] as $translation) {
                $post->translations()->where('parent_id', $post->id)->where('lang', $translation['lang'])->update($translation);
            }
            DB::commit();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return response()->json(['message' => __('app.updated_successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Post $post
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        try {
            DB::beginTransaction();
            $post->delete();
            if (Storage::exists('public/posts/' . $post->image))
                Storage::delete('public/posts/' . $post->image);
            DB::commit();
            return response()->json(['message' => __('app.deleted_successfully')]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
