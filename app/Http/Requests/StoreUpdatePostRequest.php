<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreUpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'translations' => ['required', 'array'],
            'translations.*.lang' => ['required', 'in:ar,en'],
            'translations.*.title' => ['required', 'max:255'],
            'translations.*.description' => ['required'],
            'image' => [$this->segment(2) ? 'sometimes' : 'required', 'image', 'mimes:jpg,png'],
        ];
    }
}
