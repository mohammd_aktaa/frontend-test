<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocalizeApp
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $lang = $request->cookie('app_lang', config('app.locale'));
        App::setLocale($lang ?? App::getLocale());
        return $next($request)->cookie('app_lang', App::getLocale(), 60 * 24 * 30); // One Month

    }
}
