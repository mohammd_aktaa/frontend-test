module.exports = {
  mode: 'jit',
  content: [
    "./src/**/*.{html,ts}"
  ],
  purge: [
    './src/**/*.{html,ts}',
    './src/index.html'
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
