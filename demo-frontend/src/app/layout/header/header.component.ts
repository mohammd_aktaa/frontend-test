import {Component, OnInit} from '@angular/core';
import {LanguageService} from "../../core/services/language.service";
import {AuthService} from "../../modules/auth/services/auth.service";
import {User} from "../../modules/auth/interfaces";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: User | null = null;
  isMenuOpened = false;
  constructor(private languageService: LanguageService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.user.asObservable().subscribe(user => {
      this.user = user;
    })
  }

  changeLang(lang: string) {
    this.languageService.changeLang(lang);
  }

  logout() {
    this.authService.logout().subscribe();
  }

  toggleMenu() {
    this.isMenuOpened = !this.isMenuOpened;
  }
}
