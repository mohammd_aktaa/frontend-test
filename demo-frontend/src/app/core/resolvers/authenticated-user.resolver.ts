import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {catchError, map, Observable, of} from 'rxjs';
import {AuthService} from "../../modules/auth/services/auth.service";
import {User} from "../../modules/auth/interfaces";

declare var CryptoJS: any;

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedUserResolver implements Resolve<boolean | User | null> {
  constructor(private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | User | null> {
    return this.authService.me().pipe(map(user => {
        return user;
      }),
      catchError(() => {
        return of(null);
      }));
  }
}
