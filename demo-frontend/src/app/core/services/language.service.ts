import {Inject, Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DOCUMENT} from '@angular/common';
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {AlertService} from "./alert.service";


@Injectable({
  providedIn: 'root'
})
export class LanguageService extends BaseService {

  constructor(http: HttpClient, alertService: AlertService,
              public translate: TranslateService, @Inject(DOCUMENT) private document: Document) {
    super(http, alertService);
    if (localStorage.getItem('lang') !== translate.currentLang) {
      const lang = localStorage.getItem('lang');
      translate.setDefaultLang(lang ? lang : 'en');
      this.changeLang(lang ? lang : 'en');
    }
  }


  changeLang(lang: string): void {
    this.translate.use(lang);
    this.document.getElementsByTagName('html')[0].setAttribute('lang', lang);
    this.document.getElementsByTagName('html')[0].setAttribute('dir', lang === 'ar' ? 'rtl' : 'ltr');
    localStorage.setItem('lang', lang);
    this.setCookie('app_lang', lang, 1000)
  }

  setCookie(name: string, value: string, expireDays: number, path: string = '') {
    let d: Date = new Date();
    d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
    let expires: string = `expires=${d.toUTCString()}`;
    let cpath: string = path ? `; path=${path}` : '';
    document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }
}
