import {HttpClient} from "@angular/common/http";
import {AlertService} from "./alert.service";
import {catchError, Observable, ObservableInput} from "rxjs";

export class BaseService {

  protected constructor(private http: HttpClient, protected alertService: AlertService) {
  }

  showErrorMessage(error: any, caught: ObservableInput<any>): ObservableInput<any> {

    if (error.status === 500) {
      this.alertService.show({
        text: error.error.message,
        type: 'danger',
        duration: 60000
      });
    }
    throw error;
  }

  protected post<T>(url: string, body: {}): Observable<T> {
    return this.http.post<T>(url, body).pipe(catchError(this.showErrorMessage.bind(this)))
  }

  protected delete<T>(url: string): Observable<T> {
    return this.http.post<T>(url, {_method: 'DELETE'}).pipe(catchError(this.showErrorMessage.bind(this)))
  }

  protected put<T>(url: string, body: {}): Observable<T> {
    return this.http.post<T>(url, body).pipe(catchError(this.showErrorMessage.bind(this)))
  }

  protected get<T>(url: string, params?: {}): Observable<T> {
    let query = '';
    if (params) {
      const result = [];
      for (let param in params) {
        // @ts-ignore
        result.push(encodeURIComponent(param) + '=' + encodeURIComponent(params[param]));
      }
      query = `?${result.join('&')}`
    }
    return this.http.get<T>(`${url}${query}`).pipe(catchError(this.showErrorMessage.bind(this)))
  }
}
