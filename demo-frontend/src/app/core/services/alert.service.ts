import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

interface Message {
  title?: string;
  text: string;
  duration: number; // In millisecond
  type: 'danger' | 'success' | 'warning' | 'info';
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  message = new BehaviorSubject<Message>({} as Message);

  constructor() {
  }

  show(message: Message): void {
    if (message && !Object.values(message).length) {
      this.hide();
    }
    setTimeout(() => this.message.next(message), 100);
  }

  hide(): void {
    this.message.next({} as Message);
  }
}
