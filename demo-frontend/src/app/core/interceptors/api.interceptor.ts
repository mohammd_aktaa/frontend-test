import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from "../../modules/auth/services/auth.service";
import {environment} from "../../../environments/environment";

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    let headers = new HttpHeaders();
    if (this.authService.token)
      headers = headers.append('Authorization', `Bearer ${this.authService.token}`);
    const apiReq = request.clone({
      url: request.url.startsWith('http') ? request.url : `${environment.baseURL}${request.url}`,
      headers,
      withCredentials: true
    });
    return next.handle(apiReq);
  }
}
