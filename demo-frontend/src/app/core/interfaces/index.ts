export interface Message {
  message: string;
}

export interface AccessToken {
  access_token: string;
}

export interface PaginatedData<T> {
  data: {
    entries: T[];
    "pagination": {
      "total": null,
      "last_page": null,
      "has_more": boolean,
      "current_page": null,
      "per_page": null
    }
  }
}

export interface Resource<T> {
  data: T
}
