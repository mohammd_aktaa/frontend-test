import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {Post} from "../../posts/interfaces";
import {AuthService} from "../../auth/services/auth.service";

@Directive({
  selector: '[canMakeAction]'
})
export class CanMakeActionDirective implements OnInit {
  @Input() post: Post = {} as Post;

  constructor(private elementRef: ElementRef, private authService: AuthService) {

  }

  ngOnInit(): void {
    this.elementRef.nativeElement.style.display = "none";
    this.checkAccess();
  }

  checkAccess() {
    this.elementRef.nativeElement.style.display = (this.authService.user.value && this.authService.user.value.id === this.post.user.id) ? 'block' : 'none'
  }
}
