import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Pipe({
  name: 'translateAttribute'
})
export class TranslateAttributePipe implements PipeTransform {
  constructor(private translateService: TranslateService) {
  }

  transform(value: any, attribute: string): string {
    return this.getValue(value, attribute);
  }

  getValue(value: any, attribute: string) {
    const item = value.translations.find((_: any) => _.lang === this.translateService.currentLang);
    return item ? item[attribute] : ''
  }

}
