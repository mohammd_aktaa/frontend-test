import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationComponent} from "./components/notification/notification.component";
import { CanMakeActionDirective } from './directives/can-make-action.directive';
import {TranslateModule} from "@ngx-translate/core";
import { TranslateAttributePipe } from './pipes/translate-attribute.pipe';


@NgModule({
  declarations: [NotificationComponent, CanMakeActionDirective, TranslateAttributePipe],
    imports: [
        CommonModule,
        TranslateModule
    ],
  exports: [NotificationComponent, TranslateAttributePipe, CanMakeActionDirective]
})
export class SharedModule {
}
