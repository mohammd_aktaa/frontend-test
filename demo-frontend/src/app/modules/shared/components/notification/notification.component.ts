import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AlertService} from "../../../../core/services/alert.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class NotificationComponent implements OnInit {
  type = '';
  title: string | null = '';
  text = '';
  icon = '';

  constructor(private alertService: AlertService) {
  }

  ngOnInit(): void {
    this.alertService.message.asObservable().subscribe((message) => {
      this.text = message.text;
      this.title = message.title ? message.title : '';
      this.type = message.type;
      switch (this.type) {
        case 'danger':
          this.icon = '';
          break;
        case 'success':
          this.icon = '';
          break;
        case 'info':
          this.icon = '<svg class="inline flex-shrink-0 mr-3 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>';
          break;
        case 'warning':
          this.icon = '<svg class="inline flex-shrink-0 mr-3 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">';
          break;
        default:
          this.icon = '';
          break;
      }
      if (message.duration) {
        setTimeout(() => this.alertService.hide(), message.duration);
      }
    });
  }

  hide(): void {
    this.alertService.hide();
  }
}
