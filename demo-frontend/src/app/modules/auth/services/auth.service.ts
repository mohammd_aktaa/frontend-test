import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, finalize, Observable, tap} from "rxjs";
import {User} from "../interfaces";
import {AccessToken, Message} from "../../../core/interfaces";
import {BaseService} from "../../../core/services/base.service";
import {HttpClient} from "@angular/common/http";
import {AlertService} from "../../../core/services/alert.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {
  token: string | null = '';
  user = new BehaviorSubject<User | null>(null);

  constructor(http: HttpClient, alertService: AlertService) {
    super(http, alertService);
    this.token = localStorage.getItem('access_token') ? localStorage.getItem('access_token') : '';
  }

  login(credentials: { email: string, password: string }): Observable<AccessToken> {
    return this.post<AccessToken>('login', credentials).pipe(tap((data) => {
      this.setToken(data.access_token);
      this.me().subscribe();
    }));
  }

  register(credentials: { name: string, email: string, password: string }): Observable<AccessToken> {
    return this.post<AccessToken>('register', credentials).pipe(tap((data) => {
      this.setToken(data.access_token);
      this.me().subscribe();
    }));
  }

  logout(): Observable<Message> {
    return this.post<Message>('logout', {}).pipe(finalize(() => this.revokeToken()));
  }

  me(): Observable<User> {
    return this.get<User>('user').pipe(tap((user) => this.user.next(user)), catchError((error) => {
      this.user.next(null);
      throw error;
    }));
  }

  setToken(token: string): void {
    this.token = token;
    localStorage.setItem('access_token', token);
  }

  revokeToken(): void {
    this.token = '';
    this.user.next(null);
    localStorage.removeItem('access_token');
  }
}
