import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './containers/login/login.component';
import {RegisterComponent} from './containers/register/register.component';
import {RouterModule, Routes} from "@angular/router";
import {GuestGuard} from "./guards/guest.guard";
import {ReactiveFormsModule} from "@angular/forms";
import {BaseService} from "../../core/services/base.service";
import {AuthService} from "./services/auth.service";
import {TranslateModule} from "@ngx-translate/core";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [GuestGuard]
  }
]

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        TranslateModule,
    ],
  exports: [
    RouterModule
  ],
  providers:[
    // {provide: BaseService, useExisting: AuthService}
  ]
})
export class AuthModule {
}
