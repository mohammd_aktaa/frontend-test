import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {catchError} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup
  errors: {name: string, email: string, password: string } = {name: '',email: '', password: ''}

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.registerForm = fb.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    })
  }

  ngOnInit(): void {
  }

  register() {
    if (this.registerForm.valid) {
      this.registerForm.setErrors(null);
      this.errors = {name: '',email: '', password: ''};
      this.authService.register(this.registerForm.value).pipe(catchError(({error, status}) => {
        if (status == 422) {

          for (const err in error.errors) {
            this.registerForm.controls[err].setErrors({backend: true})
            // @ts-ignore
            this.errors[err] = error.errors[err];
          }
        }
        throw error;
      })).subscribe(() => this.router.navigateByUrl('/'));
    }
  }
}
