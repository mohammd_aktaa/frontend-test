import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";
import {catchError} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  errors: { email: string, password: string } = {email: '', password: ''}

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    })
  }

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm.valid) {
      this.loginForm.setErrors(null);
      this.errors = {email: '', password: ''};
      this.authService.login(this.loginForm.value).pipe(catchError(({error, status}) => {
        if (status == 422) {

          for (const err in error.errors) {
            this.loginForm.controls[err].setErrors({backend: true})
            // @ts-ignore
            this.errors[err] = error.errors[err];
          }
        }
        throw error;
      })).subscribe(() => this.router.navigateByUrl('/'));
    }
  }

}
