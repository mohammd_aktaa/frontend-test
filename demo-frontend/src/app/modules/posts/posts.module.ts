import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IndexComponent} from './containers/index/index.component';
import {DetailsComponent} from './containers/details/details.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {MasonryGridComponent} from "./components/masonry-grid/masonry-grid.component";
import { DeleteComponent } from './components/delete/delete.component';
import {TranslateModule} from "@ngx-translate/core";
import {NgxMasonryModule} from "ngx-masonry";
import { FormComponent } from './components/form/form.component';
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: ':slug',
    component: DetailsComponent
  }
]

@NgModule({
  declarations: [
    MasonryGridComponent,
    IndexComponent,
    DetailsComponent,
    DeleteComponent,
    FormComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        TranslateModule,
        NgxMasonryModule,
        FormsModule,
    ],
  exports: [
    RouterModule
  ]
})
export class PostsModule {
}
