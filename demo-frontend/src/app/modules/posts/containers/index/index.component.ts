import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../../auth/services/auth.service";
import {PostsService} from "../../services/posts.service";
import {TranslateService} from "@ngx-translate/core";
import {Post} from "../../interfaces";
import {NgxMasonryComponent, NgxMasonryOptions} from "ngx-masonry";
import {finalize} from "rxjs";
import {User} from "../../../auth/interfaces";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  lang = '';
  page = 1;
  hasMore = false;
  loading = false;
  posts: Post[] = [];
  public masonryOptions: NgxMasonryOptions = {
    gutter: 20,
  };
  // @ts-ignore
  @ViewChild(NgxMasonryComponent) masonry: NgxMasonryComponent;
  isDeletePopupOpened = false;
  isFormPopupOpened = false;
  post: Post | null = {} as Post;
  user: User | null = null;

  constructor(private authService: AuthService, private postsService: PostsService, private translateService: TranslateService) {
    this.lang = translateService.currentLang;
    translateService.onLangChange.asObservable().subscribe((event) => {
      this.lang = event.lang;
      this.page = 1;
      this.getData();
    })
  }

  ngOnInit(): void {
    this.authService.user.asObservable().subscribe(user => {
      this.user = user;
    })
    this.getData();
  }

  getData() {
    this.loading = true;
    this.postsService.listPosts(this.page).pipe(finalize(() => this.loading = false)).subscribe(({data}) => {
      this.hasMore = data.pagination.has_more;
      this.posts = this.page === 1 ? data.entries : [...this.posts, ...data.entries];
    })
  }

  showMore() {
    ++this.page;
    this.getData();
  }

  setDeletePopupState(state: Post) {
    this.isDeletePopupOpened = false;
    setTimeout(() => {
      this.post = state;
      this.isDeletePopupOpened = true;
    })
  }

  setFormPopupState(state: Post | null) {
    this.isFormPopupOpened = false;
    setTimeout(() => {
      this.post = state;
      this.isFormPopupOpened = true;
    })
  }

  postDeleted() {
    this.post = {} as Post;
    this.page = 1;
    this.isDeletePopupOpened = false;
    this.getData();
  }

  postCreatedUpdated() {
    this.post = {} as Post;
    this.page = 1;
    this.isFormPopupOpened = false;
    this.getData();
  }
}
