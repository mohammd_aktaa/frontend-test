import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Post} from '../../interfaces';
import {PostsService} from "../../services/posts.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  slug = '';
  loaded = false;
  post: Post = {} as Post;

  constructor(private route: ActivatedRoute, private postsService: PostsService, private translateService: TranslateService) {
    route.params.subscribe((params) => {
      if (params['slug']) {
        this.slug = params['slug'];
        this.getDetails();
      }
    });
    translateService.onLangChange.asObservable().subscribe(event => {
      this.loaded = false;
      setTimeout(() => this.loaded = true)
    })
  }

  ngOnInit(): void {
  }

  getDetails() {
    this.postsService.getDetails(this.slug).subscribe(({data}) => {
      this.post = data;
      this.loaded = true;
    })
  }

}
