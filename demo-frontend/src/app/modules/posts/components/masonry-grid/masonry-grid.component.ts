import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {NgxMasonryComponent, NgxMasonryOptions} from 'ngx-masonry';
import {Post} from "../../interfaces";
import {animate, style} from "@angular/animations";

@Component({
  selector: 'app-masonry-grid',
  templateUrl: './masonry-grid.component.html',
  styleUrls: ['./masonry-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MasonryGridComponent implements OnInit, OnChanges {
  @Input() posts: Post[] = [];
  @Output() openDeleteDialogOpened = new EventEmitter(false);
  @Output() openFormDialogOpened = new EventEmitter(false);
  masonryOptions: NgxMasonryOptions = {
    gutter: 20,
    resize: true,
    animations: {
      show: [
        style({transform: 'scale(0)'}),
        animate('400ms ease-in', style({transform: 'scale(1)'})),
      ],
      hide: [
        style({transform: '*'}),
        animate('400ms ease-in', style({transform: 'scale(0)'})),
      ]
    }
  };
  // @ts-ignore
  @ViewChild(NgxMasonryComponent) masonry: NgxMasonryComponent;

  constructor(private ref: ChangeDetectorRef) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['posts'] && changes['posts'].previousValue !== changes['posts'].currentValue) {
      this.ref.detectChanges();
      setTimeout(() => {
        this.masonry.reloadItems();
        this.masonry.layout();
      }, 1000);
    }
  }

  ngOnInit(): void {
    window.addEventListener('resize', () => {
      setTimeout(() => {
        this.masonry.reloadItems();
        this.masonry.layout();
        this.ref.detectChanges();
      }, 200);
    })
  }

  openDeleteDialog(post: Post) {
    this.openDeleteDialogOpened.emit(post)
  }

  openFormDialog(post: Post) {
    this.openFormDialogOpened.emit(post)
  }
}
