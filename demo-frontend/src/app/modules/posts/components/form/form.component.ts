import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Post} from "../../interfaces";
import {PostsService} from "../../services/posts.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {catchError} from "rxjs";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 1,
        'z-index': 1,
      })),
      state('closed', style({
        opacity: 0,
        'z-index': -1,
      })),
      transition('open => closed', [
        animate('.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ]),
    ]),
  ],
})
export class FormComponent implements OnInit, OnChanges {
  @Input() post!: Post | null;
  @Input() isOpened = false;
  @Output() actionSubmitted = new EventEmitter(false);
  tab = 'english';
  errors: any = {}
  model = {
    image: null,
    translations: {
      en: {
        lang: 'en',
        title: '',
        description: '',
      },
      ar: {
        lang: 'ar',
        title: '',
        description: '',
      }
    }
  }

  constructor(private postsService: PostsService) {
  }

  get stateName() {
    return this.isOpened ? 'open' : 'closed'
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.post && this.post.id) {
      const translations = {
        en: this.post.translations.find(_ => _.lang == 'en'),
        ar: this.post.translations.find(_ => _.lang == 'ar'),
      }
      // @ts-ignore
      this.model = {...this.post};
      // @ts-ignore
      this.model.translations = translations
    }
  }

  closePopup() {
    this.isOpened = false;
  }

  ngOnInit(): void {

  }

  getActiveClass(tab: string) {
    return this.tab === tab ? 'text-blue-600 border-blue-600' : 'border-transparent hover:text-gray-600 hover:border-gray-300';
  }

  submit() {
    const data = {...this.model, translations: Object.values(this.model.translations)};
    const formData = new FormData();
    this.buildFormData(formData, data, '');
    this.errors = {};
    if (this.post && this.post.id) {
      formData.append('_method', 'PUT')
      this.postsService.update(this.post.slug, formData).pipe(catchError(({error, status}) => {
        if (status == 422) {
          this.fillErrors(error.errors);
        }
        throw error;
      })).subscribe(() => {
        this.resetModel();
        this.actionSubmitted.emit(true)
      })
    } else {
      this.postsService.create(formData).pipe(catchError(({error, status}) => {
        if (status == 422) {
          this.fillErrors(error.errors);
        }
        throw error;
      })).subscribe(() => {
        this.resetModel();
        this.actionSubmitted.emit(true)
      })

    }
  }

  fillErrors(errors: any) {
    for (const err in errors) {
      if (err.includes('.')) {
        const [attribute, index, field] = err.split('.');
        if (!this.errors[attribute]) {
          this.errors[attribute] = {}
        }
        if (!this.errors[attribute][index]) {
          this.errors[attribute][index] = {}
        }
        this.errors[attribute][index][field] = errors[err]
      } else {
        this.errors[err] = errors[err];
      }
    }
  }

  buildFormData(formData: FormData, data: any, parentKey: string) {
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
      Object.keys(data).forEach(key => {
        this.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
      });
    } else {
      const value = data == null ? '' : data;

      formData.append(parentKey, value);
    }
  }

  fileChanged(event: any) {
    const file: File = event.target.files[0];
    // @ts-ignore
    this.model.image = file;
  }

  resetModel() {
    this.model = {
      image: null,
      translations: {
        en: {
          lang: 'en',
          title: '',
          description: '',
        },
        ar: {
          lang: 'ar',
          title: '',
          description: '',
        }
      }
    }
  }
}
