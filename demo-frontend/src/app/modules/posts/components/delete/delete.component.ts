import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostsService} from "../../services/posts.service";
import {Post} from "../../interfaces";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        opacity: 1,
        'z-index': 1,
      })),
      state('closed', style({
        opacity: 0,
        'z-index': -1,
      })),
      transition('open => closed', [
        animate('.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ]),
    ]),
  ],
})
export class DeleteComponent implements OnInit {
  @Input() post!: Post | null;
  @Input() isOpened = false;
  @Output() rowDeleted = new EventEmitter(false);

  constructor(private postsService: PostsService) {
  }

  get stateName() {
    return this.isOpened ? 'open' : 'closed'
  }

  ngOnInit(): void {
  }

  closePopup() {
    this.isOpened = false;
  }

  delete() {
    if (this.post)
      this.postsService.deletePost(this.post.slug).subscribe(() => {
        this.closePopup();
        this.rowDeleted.emit(true);
      })
  }
}
