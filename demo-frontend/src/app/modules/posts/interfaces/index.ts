import {User} from "../../auth/interfaces";

interface Translations {
  lang: string;
  title: string;
  description: string;
}

export interface Post {
  id: number;
  image: string;
  slug: string;
  image_url: string;
  user: User;
  translations: Translations[];
}
