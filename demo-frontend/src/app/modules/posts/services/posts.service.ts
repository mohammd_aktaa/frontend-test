import {Injectable} from '@angular/core';
import {BaseService} from "../../../core/services/base.service";
import {HttpClient} from "@angular/common/http";
import {AlertService} from "../../../core/services/alert.service";
import {Message, PaginatedData, Resource} from "../../../core/interfaces";
import {Post} from "../interfaces";
import {Observable, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostsService extends BaseService {

  constructor(http: HttpClient, alertService: AlertService) {
    super(http, alertService);
  }

  listPosts(page = 1, perPage = 20): Observable<PaginatedData<Post>> {
    return this.get<PaginatedData<Post>>('posts', {page, perPage})
  }

  deletePost(slug: string): Observable<Message> {
    return this.delete<Message>(`posts/${slug}`)
  }

  create(body: any): Observable<Message> {
    return this.post<Message>(`posts`, body).pipe(tap(data => {
      this.alertService.show({
        text: data.message,
        type: 'success',
        duration: 5000
      })
    }))
  }

  update(slug: string, body: any): Observable<Message> {
    return this.put<Message>(`posts/${slug}`, body).pipe(tap(data => {
      this.alertService.show({
        text: data.message,
        type: 'success',
        duration: 5000,
      })
    }))
  }

  getDetails(slug: string): Observable<Resource<Post>> {
    return this.get<Resource<Post>>(`posts/${slug}`)
  }
}
