<?php


return [
    'saved_successfully' => 'Saved Successfully!',
    'updated_successfully' => 'Updated Successfully!',
    'deleted_successfully' => 'Deleted Successfully!',
];
