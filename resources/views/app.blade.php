<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <base href="/" />

    {{-- This'll load our extracted and hashed CSS assets here --}}
    @env('production')
        @if (isset($assets) && count($assets))
            <link rel="stylesheet" href="/front/{{ $assets['styles'] }}">
        @endif
    @endenv
</head>
<body>

<app-root></app-root>

{{-- This'll load our hashed assets when in production --}}
@env('production')
    @if (isset($assets) && count($assets))
        <script src="/front/{{ $assets['runtime'] }}" defer></script>
        <script src="/front/{{ $assets['polyfills'] }}" defer></script>
        <script src="/front/{{ $assets['main'] }}" defer></script>
    @endif
    {{-- This'll load the development assets when in dev mode --}}
@else
    <script src="/front/runtime.js" defer></script>
    <script src="/front/polyfills.js" defer></script>
    <script src="/front/styles.js" defer></script>
    <script src="/front/vendor.js" defer></script>
    <script src="/front/main.js" defer></script>
@endenv
</body>
</html>
